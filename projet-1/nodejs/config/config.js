var config = {};

config.app = "Nuit de l'info";
config.url = 'http://localhost/';

config.rss = {
  title:        "Application pour la nuit de l'info",
  description:  "Api pour la nuit de l'info",
  copyright:    "Jean-Baptiste Dominguez pour la nuit de l'info"
};

module.exports = config;