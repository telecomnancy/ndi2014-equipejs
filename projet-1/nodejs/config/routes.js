/**
 * controllers
 */
var userCtrl = require('../controllers/user.js');
var victimCtrl = require('../controllers/victim.js');
var zoneCtrl = require('../controllers/zone.js');
var orgaCtrl = require('../controllers/organization.js');
var secret = require('../config/secret');
var jwt = require('express-jwt');
/**
* hellpers
*/

var http = {
  _501: function(req, res, next) {
    res.status(501).send('501 Not Implemented Yet');
  },
  _404: function(req, res, next) {
    res.status(404).send('404 Not Found');
  },
  _405: function(req, res, next) {
    res.status(405).send('405 Method Not Allowed');
  },
};

var routes = require('express-routes');
module.exports = function(app) {
  var all_routes = [
    {
      name: 'Home',
      pattern: '',
      get: function(req, res, next) {
        res.send("Welcome");
      },
    },
    {
      name : 'User - Auth',
      pattern : 'signin/',
      post : userCtrl.auth,
    },

      

    {
      name : 'User - List',
      pattern : 'users/',
      get : userCtrl.get
    },
    // {
    //   name : 'User - Sign',
    //   pattern : 'signup/:identity/:password',
    //   get : userCtrl.signIn
    // },

    /**
     * VICTIMS
     */
    {
      name: 'Victims - List',
      pattern: 'victims/',
      get: victimCtrl.get,
      post: victimCtrl.create,
    },

    {
      name: 'Victim - CRUD',
      pattern: 'victims/:id',
      get: victimCtrl.read,
      put: victimCtrl.update,
      del: victimCtrl.delete,
    },

    {
      name: 'Victims - Zone',
      pattern: 'victims/:id/zone',
      put: victimCtrl.updateZone,
    },

    {
      name: 'Victims - Events',
      pattern: 'victims/:id/events',
      post: victimCtrl.createEvent,
    },

    {
      name: 'Victims - Services',
      pattern: 'victims/:id/services',
      post: victimCtrl.createService,
    },

    {
      name: 'Victims - Messages',
      pattern: 'victims/:id/messages',
      post: victimCtrl.createMessage,
    },

    /**
     * ZONES
     */
    {
      name: 'Zones - List',
      pattern: 'zones/',
      get: zoneCtrl.get,
      post: zoneCtrl.create,
    },

    {
      name: 'Zone - CRUD',
      pattern: 'zones/:id',
      get: zoneCtrl.read,
      put: zoneCtrl.update,
      del: zoneCtrl.delete,
    },

    {
      name: 'Zone - Rule',
      pattern: 'zones/:id/rules',
      post: zoneCtrl.createRule,
    },

    {
      name: 'Zone - Rule',
      pattern: 'zones/:id/rules/:ruleId',
      del: zoneCtrl.deleteRule,
    },

    {
      name: 'Zone - Stocks',
      pattern: 'zones/:id/stocks',
      post: zoneCtrl.createStock,
    },

    {
      name: 'Zone - Stock',
      pattern: 'zones/:id/stocks/:name',
      put: zoneCtrl.updateStock,
    },

    /**
     * ORGANIZATIONS
     */
    {
      name: 'Organizations - List',
      pattern: 'organizations/',
      get: orgaCtrl.get,
      post: orgaCtrl.create,
    },

    {
      name: 'Organization - CRUD',
      pattern: 'organizations/:id',
      get: orgaCtrl.read,
      put: orgaCtrl.update,
      del: orgaCtrl.delete,
    },


    {
      name: 'List all routes',
      pattern: 'docs/',
      get: function(req, res, next) {
        var routes_array = [];

        for(i in all_routes) {
          var route = all_routes[i];
          var verb = ['get','post','put','del'];
          route['methods'] = [];
          for(k in verb) {
            if(verb[k] in route) {
              route['methods'].push(verb[k]);
            }
            if('all' in route) {
              route['methods'] = verb;
            }
          }

          routes_array.push(route);
        }

        res.send("<pre>" + JSON.stringify(routes_array, null, '\t') + "</pre>");
      }
    },

    {
      name: 'Default routes',
      pattern: '*',
      all: http._405
    },

  ]

  routes.register(all_routes);
}
