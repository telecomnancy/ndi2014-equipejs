var express = require('express');
var app = express();
var bodyParser = require('body-parser'); //bodyparser + json + urlencoder
var orm = require('orm');
var bcrypt = require('bcrypt');
var mongoose = require('mongoose');
var path = require("path");
var routes = require('express-routes');
mongoose.connect('mongodb://localhost/api');
require('./models/user');
require('./models/organization');
require('./models/victim');
require('./models/zone');
app.listen(80);
app.use(bodyParser());
var router = express.Router;
var secret = require('./config/secret')
var jwt = require('express-jwt')
var userCtrl = require('./controllers/user')
// app.use(orm.express("mysql://api:api@localhost/api", {
//     define: function (db, models, next) {
//         /**
//          * Modèle user
//          */
//         models.user = db.define("user", {
//             id: {type: 'serial', key: true},
//             mail: {type: 'text'},
//             password: {type: 'text'},
//             created: Date,
//         }, {
//             methods: {
//                 comparePassword: function(password, cb) {
//                     bcrypt.compare(password, this.password, function(err, isMatch) {
//                         if (err) return cb(err);
//                         cb(isMatch);
//                     });
//                 }
//             }
//         });

//         /**
//          *  Modèle victime
//          */
//         models.victim = db.define('victim', {
//             id : {type : 'serial', key : true},
//             identity : { type : }
//         }, {

//         });

//         /**
//          *  Modèle product
//          */
//         models.product = db.define('product', {
//             id : {type : 'serial', key : true},
//             name : {type : 'text'},
//             type : {type : 'text'},
//             meta : {type : 'text'}
//         });

//         /**
//          * Modèle groupe de user
//          */
//         models.groupe = db.define("groupe", {
//             id: {type: 'serial', key: true},
//             nom: {type: 'text'}
//         });


//         /**
//          * Modèle news
//          */
//         models.news = db.define("news", {
//             id: {type: 'serial', key: true},
//             title: {type: 'text'},
//             content: {type: 'text'},
//             created: Date
//         });

//         /**
//          * Dépendance entre user et groupe
//          */
//         models.user.hasMany('groupes', models.groupe, {}, {autoFetch: true});
//         next();
//     }
// }));



app.all('*', function(req, res, next) {
  res.set('Access-Control-Allow-Origin', '*');
  res.set('Access-Control-Allow-Credentials', true);
  res.set('Access-Control-Allow-Methods', 'GET, POST, DELETE, PUT');
  res.set('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Authorization');
  if ('OPTIONS' == req.method) return res.send(200);
  next();
});

//TODO: DELETE ME!!!
app.use('/!/', express.static(__dirname + '/TEST'));
app.use('/', express.static(__dirname + '/../angularjs/'));
//Configuration du routeur
var router = express.Router();
//At each request
router.use(function(req, res, next){
    //Display the request
    console.log(req.method, req.url, new Date());
    res.locals.session = req.session;
    next();
});

/* require('./config/routes')(router);
app.use('/', router); */

routes(app, {
  // directory where the helper will look for route files automatically (optional)
  directory: [path.join(__dirname, 'config')],
  prefix: '/',
});
app.get('/users/me/', jwt({secret : secret.secretToken}), userCtrl.me);

console.log('Server starting on port 1337');
