//Charge mongoose
var mongoose = require('mongoose');
//Schema constructor
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

//Zone schema
var Zone = new Schema({
    name: {type: String},
    //Polygon
    loc: {type: Object, index: '2dsphere'},
    type: {type: String},
    stocks: [{
        name: {type: String},
        type: {type: String},
        quantity: {type: Number}
    }],
    organizations: [{type: ObjectId, ref: 'Organization'}],
    personals: [{type: ObjectId, ref: 'User'}],
    rules: [{priority: {type: String}, consign: {type: String}}] 
});


//Export model
module.exports = mongoose.model('Zone', Zone);
