//Charge mongoose
var mongoose = require('mongoose');
//Schema constructor
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;
//Victim schema
var Victim = new Schema({
    identity: {type : String, required : true},
    nationality: {type: String},
    //Coordonnées
    loc: {
        type: Object,
        index: '2dsphere'
    },
    zone: {type: ObjectId, ref: 'Zone'},
    birthdate: {type: String},
    events: [{
        name: {type: String, required: true},
        date: {type: Date}
    }],
    services: [{
        name: {type: String, required: true},
        type: {type: String},
        date: {type: String},
        quantity: {value: Number, unit: String},
        provider: {user: {type: ObjectId, ref: 'User', name: String}, organization: {type: ObjectId, ref: 'Organization', name: String}}
    }],
    messages: [{
        author: {type: String, required: true},
        content: {type: String, required: true},
        date: {type: String, required: true},
    }]
});


//Export model
module.exports = mongoose.model('Victim', Victim);
