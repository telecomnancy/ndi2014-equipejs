//Charge mongoose
var mongoose = require('mongoose');
//Schema constructor
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

//Organization schema
var Organization = new Schema({
    name: {type : String},
    type: {type : String},
    admins: [{type : ObjectId, ref : 'User'}],
});


//Export model
module.exports = mongoose.model('Organization', Organization);
