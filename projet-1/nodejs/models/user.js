//Charge mongoose
var mongoose = require('mongoose');
//Schema constructor
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

//User schema
var User = new Schema({
    identity : {type : String},
    competence : {type : String},
    password : {type : String},
    role : {type : String},
    organizations : [{name : String, type : ObjectId, ref : 'Organization'}]
});


//Export model
module.exports = mongoose.model('User', User);