var models = ['victims', 'zones', 'organizations', 'rules']

for(i in models) {
describe("'" + models[i] +"' API", function() {

    beforeEach(function() {
        $.ajaxSetup({
            async: false,
            dataType: "json"
        });
    });

    it("Should not be able to put on /" + models[i] +"/", function() {
        var statusCode = $.ajax("/" + models[i] +"/", { method : 'PUT'} ).status;

        expect(statusCode).toBe(405);
    });

    it("Should not be able to delete on /" + models[i] +"/", function() {
        var statusCode = $.ajax("/" + models[i] +"/", { method : 'DELETE'} ).status;

        expect(statusCode).toBe(405);
    });


    it("Should not be able to post on /" + models[i] +"/:id", function() {
        var statusCode = $.ajax("/" + models[i] +"/0", { method : 'POST'} ).status;

        expect(statusCode).toBe(405);
    });

    it("Should be able to get on /" + models[i] +"/", function() {
        var statusCode = $.ajax("/" + models[i] +"/", { method : 'GET'} ).status;

        expect(statusCode).toBe(200);
    });

    /*it("Should not be able to post bad content on /" + models[i] +"/", function() {
        var objectSettings = { method : 'POST',
                               data : { title : "Hi!",
                                        message : "I do an unit test !",
                                        auteur : "Jasmine",
                                        image : "/toto.png"}
                             };

        var data = $.ajax("/" + models[i] +"/", objectSettings);
        expect(data.status).toBe(400);
    });*/

});
}
