var Organization  = require('../models/organization');
exports.create = function(req, res){
    var type = req.body.type || '';
    var name = req.body.name || '';
    if(type === '' || name ===''){
        return res.end();
    }
    var organization = new Organization();
    organization.type = type;
    organization.name = name;
    organization.save(function(err, result){
        if(err){
            console.log(err);
            return  res.status(500).end('Server error');
        }
        return res.status(200).send('OK');
    })
}

exports.read = function(req, res){
    var id = req.param('id') || '';
    if(id === ''){
        return res.end();
    }
    Organization.findOne({'_id' : id}, function(err, organization){
        if(err){
            console.log(err);
            return res.status(500).end('Server error');
        }
        return res.end(JSON.stringify(organization));
    });
}
exports.update = function(req, res){
    res.end('Pas encore fait');
}
exports.delete = function(req, res){
    var id = req.param('id') || '';
    if(id === ''){
        return res.end(400);
    }
    Organization.remove({'_id': id}, function(err, result){
        if(err){
            console.log(err);
            return res.status(500).end('Server error');
        }
        return res.status(200).end('OK');
    });
}

exports.get = function(req, res){
    var name = req.param('name') || '';
    if(name === ''){
        Organization.find({}, function(err, organizations){
            if(err){
                console.log(err);
                return res.send(500).end('Server error');
            }
            return res.end(JSON.stringify(organizations));
        });
    }else{
        Organization.findOne({name : name}, function(err, organization){
            if(err){
                console.log(err);
                return res.send(500).end('Server error');
            }
            return res.send(JSON.stringify(organization));
        });
    }

}

exports.addUser = function(req, res){
    var id = req.param('id') || '';
    var userId = req.body.userId || '';
    if(id === '' || userId === ''){
        return res.send(400);
    }
    Organization.findOne({'_id': id}, function(err, organization){
        if(err){
            console.log(err);
            return res.send(500).end('Server error');
        }
        organization.users.push(userId);
        organization.save(function(err, result){
            if(err){
                console.log(err);
                return res.send(500).end('Server error');
            }
            return res.send(200).end('OK');
        })
    })
}