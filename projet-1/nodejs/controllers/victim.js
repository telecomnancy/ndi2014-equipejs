var Victim = require('../models/victim');
var onlyfor = require('../utils/onlyfor');

//Création d'une victime
exports.create = function(req, res){
    var identity = req.body.identity || '';
    var nationality = req.body.nationality || '';
    var birthdate = req.body.birthdate || '';
    var loc = req.body.loc || '';

    if(identity === ''){
        return res.status(400).end('400 Bad Request');
    }
    //Création
    var victim = new Victim();
    victim.identity = identity;
    if(birthdate != ''){
        victim.birthdate = birthdate;
    }
    if(loc != ''){
        victim.loc = loc;
    }
    if(nationality != ''){
        victime.nationality = nationality;
    }
    //Sauvegarde
    victim.save(function(err, result){
        if(err){
            console.log(err);
            return res.status(500).end('Server error');
        }
        res.status(201).end('201 CREATED');
    });
}

//Récupération d'une victime
exports.read = function(req, res, next){
    var id = req.param('id') || '';
    if(id === ''){
        return res.status(400).end('400 Bad Request');
    }
    Victim.findOne({'_id': id}, function(err, victims){
        if(err){
            console.log(err);
            return  res.status(500).end('Server error');
        }
        return res.status(200).end(JSON.stringify(victims));
    });

}

//Mise à jour d'une victime
exports.update = function(req, res){
    var id = req.param('id') || '';
    var loc = req.body.loc || '';
    var birthdate = req.body.birthdate || '';
    var nationality = req.body.nationality || '';

    if(id === '' || loc === '' || birthdate === ''){
        return res.status(400).end('400 Bad Request');
    }
    console.log(loc);
    Victim.update({'_id': id}, {$set: {loc: loc, birthdate: birthdate, nationality: nationality}}, function(err, result){
        if(err){
            console.log(err);
            return res.status(500).end('Server error');
        }
        return res.status(200).end('OK');
    });
}

//Suppresion d'une victime
exports.delete = function(req, res){
    var id = req.param('id') || '';
    if(id === ''){
        return res.status(400).end('400 Bad Request');
    }
    Victim.remove({'_id' : id}, function(err, result){
        if(err){
            console.log(err);
            return res.status(500).end('Server error');
        }
        return res.status(200).end('OK');
    })
}

exports.get = function(req, res, next){
    var identity = req.param('identity') || '';
    if(identity === ''){
        Victim.find({}, function(err, victims){
            if(err){
                console.log(err);
                return res.status(500).end('Server error');
            }
            return res.end(JSON.stringify(victims));
        });
    }else{
        Victim.find({identity : new RegExp('^.*' + identity + '.*$', "i")}, function(err, victim){
            if(err){
                console.log(err);
                return res.status(500).end('Server error');
            }
            return res.end(JSON.stringify(victim));
        });
    }

}

exports.updateZone = function(req, res){
    var id = req.body.idVictim || '';
    var zoneId = req.body.idZone || '';
    var loc = req.body.loc || '';
    if(id === '' || zoneId === '' || loc === ''){
        return res.status(400).end('400 Bad Request');
    }
    Victim.findOne({'_id' : id}, function(err, victim){
        if(err){
            console.log(err);
            return res.status(500).end('Server error');
        }
        victim.zone = zoneId;
        victim.loc = loc;
        victim.save(function(err, result){
            if(err){
                console.log(err);
                return res.status(500).end('Server error');
            }
        });
    });
}

//Ajout d'un évènement à une victime
exports.createEvent = function(req, res){
    var id = req.param('id') || '';
    var eventName = req.body.name || '';
    var eventDate = req.body.date || '';
    if(id === '' || eventName === '' || eventDate === ''){
        return res.status(400).end('400 Bad Request');
    }
    Victim.findOne({'_id' : id}, function(err, victim){
        if(err){
            console.log(err);
            return res.status(500).end('Server error');
        }
        victim.events.push({name: eventName, date: eventDate});
        victim.save(function(err, result){
            if(err){
                console.log(err);
                return res.status(500).end('Server error');
            }
            res.status(200).end('OK');
        });
    });
}

//Ajout d'un service à une victime
exports.createService = function(req, res){
        var id = req.param('id') || '';
        var serviceName = req.body.name || '';
        var serviceType = req.body.type || '';
        var serviceDate = req.body.date || '';
        if(id === '' || serviceName === '' || serviceType === '' || serviceDate === ''){
            return res.status(400).end('400 Bad Request');
        }
        Victim.findOne({'_id': id}, function(err, victim){
        if(err){
                console.log(err);
                return res.status(500).end('Server error');
        }
        victim.services.push({name: serviceName, type: serviceType, date: serviceDate});
        victim.save(function(err, result){
                if(err){
                        console.log(err);
                        return res.status(500).end('Server error');
                }
                res.status(200).end('OK');
            });
        });
}

//Ajout d'un message à une victime
exports.createMessage = function(req, res){
        var id = req.param('id') || '';
        var messageAuthor = req.body.author || '';
        var messageContent = req.body.content || '';
        var messageDate = new Date();
        if(id === '' || messageAuthor === '' || messageContent === ''){
            return res.status(400).end('400 Bad Request');
        }
        Victim.findOne({'_id': id}, function(err, victim){
        if(err){
                console.log(err);
                return res.status(500).end('Server error');
        }
        victim.message = victim.message || [];
        victim.message.push({author: messageAuthor , content: messageContent, date: messageDate});
        console.log(victim.message);
        victim.save(function(err, result){
                if(err){
                        console.log(err);
                        return res.status(500).end('Server error');
                }
                res.status(200).end('OK');
            });
        });
}
