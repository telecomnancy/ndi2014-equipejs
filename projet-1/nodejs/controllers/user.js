var jwt = require('jsonwebtoken');
var bcrypt = require('bcrypt');
var tokenManager = require('../config/token_manager');
var secret = require('../config/secret');
var SALT_WORK_FACTOR = 10;
var User = require('../models/user');
var onlyfor = require('../utils/onlyfor');

exports.auth = function(req, res) {
    var identity = req.param('identity') || '';
    var password = req.body.password || '';
    if(identity === '' || password === ''){

        return res.send(400).end('Bad request');
    }
    User.findOne({identity : identity}, function(err, user){
        if(err){
            console.log(err);
            return res.send(500).end('Server error');
        }
        if(user == null){

            return res.send(400).end('Bad request');
        }
        if(password != user.password){
            return res.send(400).end('Bad request');
        }
        var data = {'_id' : user._id};
        var token = jwt.sign(data, secret.secretToken, {expiresInMinutes : tokenManager.TOKEN_EXPIRATION});
        return res.json({token : token});
    });
    // console.log(req.body);
    // req.models.user.find({mail:req.body.mail}, function(err, users) {
    //     if (err) return res.send(500);
    //     if (users.length == 0) return res.send(400);
    //     users[0].comparePassword(req.body.password, function(isMatch) {
    //         if (!isMatch) {
    //             return res.send(401);
    //         }
    //         *
    //          * Ici on enregistre les droits lors de la création du token
    //          * Genre, {id: users[0], roles: users[0].roles}
             
    //         var groupes = [];
    //         for(var i=0; i<users[0].groupes.length; ++i) {
    //             groupes.push(users[0].groupes[i].nom);
    //         };
    //         var data = {id: users[0].id, groupes:groupes};
    //         console.log(data);
    //         var token = jwt.sign(data, secret.secretToken, { expiresInMinutes: tokenManager.TOKEN_EXPIRATION });
    //         return res.json({token:token});
    //     });
    // });
};

exports.me = function(req, res, next){
    // onlyfor.admin(req, res, next);
    console.log(req.user);
    res.end(''); 
}

exports.get = function(req, res){
    User.find(function(err, users){
        if(err){
            console.log(err);
            return res.send(500).end('Server error');
        }
        return res.end(JSON.stringify(users));
    })
}

exports.signIn = function(req, res) {
    var identity = req.param('identity') || '';
    var password = req.param('password') || '';
    if(identity === '' || password === ''){
        return res.send(400).end('Bad request');
    }
    User.find({identity : identity}, function(err, users){
        if(err){
            console.log(err);
            return res.send(500).end('Server error');
        }
        if(users.length !== 0){
            return res.end('Already in base');
        }
        var user = new User();
        user.identity = identity;
        user.password = password;
        user.save(function(err, result){
            if(err){
                console.log(err);
                return res.send(500).end('Server error');
            }
            return res.send(200).end('OK');
        })
    });
    // req.models.user.find({identity:req.body.identity}, function(err, users) {
        
    //     if (err) return res.send(500);
    //     if (users.length > 0) return res.send(400);
        
    //     var newRecord = {};
    //     newRecord.mail = req.body.mail;
    //     newRecord.password = req.body.password;
        
    //     bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
    //         if (err) return res.send(500);
    //         bcrypt.hash(newRecord.password, salt, function(err, hash) {
    //             if (err) return res.send(500);
    //             newRecord.password = hash;
    //             req.models.user.create(newRecord, function(err, results) {
    //                 if (err) return res.send(500);
    //                 return res.send(200);
    //             });
    //         });
    //     });
        
        
    // });
};
