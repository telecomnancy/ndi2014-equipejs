var Zone  = require('../models/zone');


exports.create = function(req, res){
    var type = req.body.type || '';
    var name = req.body.name || '';
    var loc = req.body.loc || '';
    if(type === '' || name === '' || loc === ''){
        return res.status(400).end('400 Bad Request');
    }
    var zone = new Zone();
    zone.type = type;
    zone.name = name;
    zone.loc = loc;
    console.log(loc);
    zone.save(function(err, result){
        if(err){
            console.log(err);
            return  res.status(500).end('Server error');
        }
        return res.status(201).end('201 CREATED');
    })
}



exports.read = function(req, res){
    var id = req.param('id') || '';
    if(id === ''){
        return res.status(400).end('400 Bad Request');
    }
    Zone.findOne({'_id' : id}, function(err, zone){
        if(err){
            console.log(err);
            return res.status(500).end('Server error');
        }
        return res.end(JSON.stringify(zone));
    });
}

//Mise à jour d'une victime
exports.update = function(req, res){
    var id = req.param('id') || '';
    var name = req.body.name || '';
    var type = req.body.type || '';
    var loc = req.body.loc || '';
    if(id === '' || loc === '' || type === '' || name === ''){
        return res.status(400).end('400 Bad Request');
    }
    console.log(loc);
    Zone.update({'_id': id}, {$set: {loc: loc, name : name, type : type}}, function(err, result){
        if(err){
            console.log(err);
            return res.status(500).end('Server error');
        }
        return res.status(200).end('OK');
    });
}

//Suppresion d'une zone
exports.delete = function(req, res){
    var id = req.param('id') || '';
    if(id === ''){
        return res.status(400).end('400 Bad Request');
    }
    Zone.remove({'_id' : id}, function(err, result){
        if(err){
            console.log(err);
            return res.status(500).end('Server error');
        }
        return res.status(200).end('OK');
    })
}

exports.get = function(req, res){
    var type = req.param('type') || '';
    var name = req.param('name') || '';
    var lat = req.param('lat') || '';
    var lon = req.param('long') || '';
    if(type === '' && name === '' && lat === '' && lon == ''){
        Zone.find(function(err, zones){
            if(err){
                console.log(err);
                return res.status(500).end('Server error');
            }
            return res.status(200).end(JSON.stringify(zones));
        })
    }else if(type !== ''){
        Zone.findOne({'type': type}, function(err, zone){
            if(err){
                console.log(err);
                return res.status(500).send('Server error');
            }
            return res.status(200).send(JSON.stringify(zone));
        });
    }else if(name !== ''){
        Zone.findOne({'name': name}, function(err, zone){
            if(err){
                console.log(err);
                return res.status(500).send('Server error');
            }
            return res.status(200).send(JSON.stringify(zone));
        });
    }else if(lat != '' && lon != ''){
        Zone.find({loc:{"$geoIntersects": {"$geometry": {type: 'Point', coordinates: [parseFloat(lat), parseFloat(lon)]}}}}, function(err, zones){
            if(err){
                console.log(err);
                return res.send(500).end('Server error');
            }
            return res.end(JSON.stringify(zones));
        });
    }
}

exports.createRule = function(req, res){
    var id = req.param('id') || '';
    var priority = req.body.priority || '';
    var consign = req.body.consign || '';
    var distance = req.body.distance || '';
    if(id === '' || priority === '' || consign === '' || distance === ''){
        return res.status(400).end('400 Bad Request');
    }
    var rule = {priority : priority, consign : consign, distance : distance};
    Zone.findOne({'_id': id}, function(err, zone){
        if(err){
            console.log(err);
            return res.status(500).end('Server error');
        }
        zone.rules.push(rule);
        zone.save(function(err, result){
            if(err){
                console.log(err);
                return res.status(500).end('Server error');
            }
            res.status(200).end('OK');
        })
    });
}


exports.createStock = function(req, res){
    var id = req.param('id') || '';
    var name = req.body.stock.name || '';
    var type = req.body.stock.type || '';
    var quantity = req.body.stock.quantity || '';
    if(id === '' || name === '' || type === '' || quantity === ''){
        return res.status(400).end('400 Bad Request');
    }
    var stock = {name : name, type : type, quantity : quantity};
    Zone.findOne({'_id': id}, function(err, zone){
        if(err){
            console.log(err);
            return res.status(500).end('Server error');
        }
        zone.stocks.push(stock);
        zone.save(function(err, result){
            if(err){
                console.log(err);
                return res.status(500).end('Server error');
            }
            return res.status(200).send('OK');
        });
    });
}

exports.updateStock = function(req, res){
    var id = req.param('id') || '';
    var quantity = req.body.quantity || '';
    var name = req.param('name') || '';
    if(id === '' || quantity === '' || name === ''){
        return res.status(400).end('400 Bad Request');
    }
    Zone.findOne({'_id': id}, function(err, zone){
        if(err){
            console.log(err);
            return res.status(500).end('Server error');
        }
        for(i in zone.stock){
            if(zone.stock[i].name === name){
                zone.stock[i].name -= quantity;
                break;
            }
        }
        res.status(200).send('OK');
    })
}

exports.deleteRule = function(req, res){
    var id = req.param('id') || '';
    var ruleId = req.param('ruleId') || '';
    if(id === '' || ruleId === ''){
        return res.status(400).end('400 Bad Request');
    }
    Zone.findOne({'_id' : id}, function(err, zone){
        if(err){
            console.log(err);
            return res.status(500).end('Server error');
        }
        zone.rule.remove(ruleId);
        return res.status(200).end('OK');
    });
}

exports.deleteStock = function(req, res){

}
