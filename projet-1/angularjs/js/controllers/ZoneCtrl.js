/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

appControllers.controller('ZoneCtrl', function($scope, $rootScope, $location, $routeParams, ZoneService) {
    
    $scope.types = types_zone_index;
    $scope.types_zone = types_zone;
    
    $scope.loadZone = function() {
            $rootScope.showLoading();
        ZoneService.get($routeParams.id).success(function(zone) {
                $scope.zone = zone;
                if(typeof $scope.mapLocalisation === 'undefined')
                    $scope.initMap();
                $rootScope.hideLoading();
            }).error(function() {
                $rootScope.hideLoading();
        });
    };
    
    $scope.updateZone = function(zone) {
            $rootScope.showLoading();
        ZoneService.update(zone).success(function() {
            /**
            * affiche l'icone de chargement
            */
            $rootScope.hideLoading();
            
            $location.path("/zone");
            
            }).error(function() {
            $rootScope.hideLoading();
        });
    };
    
    $scope.createZone = function(zone) {
        zone.loc = { "type": "Polygon", "coordinates": $scope.listLatLng };
        ZoneService.create(zone).success(function() {
            $location.path("/zone");
        }).error(function() {
            $rootScope.hideLoading();
        });
    };
    
    $scope.initMap = function () {
        
        $scope.points = [];
        $scope.listLatLng = [];
        $scope.coordinates = [0, 0];
        
        $scope.mapLocalisation = L.map('mapLocalisation', {
            center: $scope.coordinates,
            zoom: 5
        });

        L.tileLayer('http://{s}.tiles.mapbox.com/v3/examples.map-i875mjb7/{z}/{x}/{y}.png', {
            attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
            maxZoom: 18
        }).addTo($scope.mapLocalisation);
        

        $scope.mapLocalisation.on('click', function(e) {

            /**
             * 
             */
            $scope.point=L.marker(e.latlng, {
                icon: $rootScope.icons['circle']
            });

            /**
             * 
             */
            $scope.points.push($scope.point);
            var point = [$scope.point.getLatLng().lat, $scope.point.getLatLng().lng];
            $scope.listLatLng.push(point);

            
            if($scope.listLatLng.length > 2) {
                if(typeof $scope.polygon === 'undefined') {
                    $scope.polygon = L.polygon($scope.listLatLng);
                    $scope.polygon.addTo($scope.mapLocalisation);
                }
                $scope.polygon.setLatLngs($scope.listLatLng);
                $scope.zone.loc = { "type": "Polygon", "coordinates": $scope.listLatLng };
            }

            /**
             * 
             */
            $scope.point.addTo($scope.mapLocalisation);
        });
        
        $scope.resetPolygon = function () {
            $scope.points = [];
            $scope.listLatLng = [];
        };
        
    };
});
