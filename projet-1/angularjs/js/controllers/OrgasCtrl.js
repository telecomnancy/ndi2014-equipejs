/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

appControllers.controller('OrgasCtrl', function ($scope, $rootScope, OrganizationService) {
    /**
     * initialisation zones
     */
    $scope.orgas = [];
    
    $scope.loadOrga = function () {
        OrganizationService.getAll().success(function (orgas) {
            console.log(orgas)
            $scope.orgas = orgas;
        }).error(function () {
        });
    };
    
    $scope.deleteOrga = function (index) {
        OrganizationService.delete($scope.orgas[index]._id).success(function () {
            $scope.loadOrga();
        }).error(function () {
        });
    };
    
    $scope.loadOrga();
    
});