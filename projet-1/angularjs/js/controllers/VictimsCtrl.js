/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

appControllers.controller('VictimsCtrl', function ($scope, $rootScope, VictimService) {
    /**
     * initialisation victims
     */
    $scope.victims = [];
    
    $scope.loadVictim = function () {
        
        /**
         * Icone de chargement
         */
        $rootScope.showLoading();

        VictimService.getAll().success(function (victims) {
            $rootScope.showLoading();
            $scope.victims = victims;
            $rootScope.hideLoading();
        }).error(function () {
            $rootScope.hideLoading();
        });
        
    };
    
    $scope.deleteVictim = function (index) {
            $rootScope.showLoading();
        VictimService.delete($scope.victims[index]._id).success(function () {
            $scope.loadVictim();
            $rootScope.hideLoading();
        }).error(function () {
            $rootScope.hideLoading();
        });
    };
    
    $scope.loadVictim();
});


