/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



appControllers.controller('VictimCtrl', function($scope, $rootScope, $location, $routeParams, VictimService) {
    
    $scope.victim = {};
    
    
    $scope.loadVictim = function(canUpdateLoc) {
        $rootScope.showLoading();
        $scope.canUpdateLoc = (canUpdateLoc);
        VictimService.get($routeParams.id).success(function(victim) {
            $scope.victim = victim;
            $scope.initMap();
            $rootScope.hideLoading();
            }).error(function() {
            $rootScope.hideLoading();
        });
    };
    
    $scope.sendMess = function(data) {
        $rootScope.showLoading();
        VictimService.sendMess($scope.victim._id, data).success(function() {
            $rootScope.hideLoading();
            $scope.event={};
            $scope.loadVictim();
        }).error(function() {
            $rootScope.hideLoading();
        });
    };
    
    $scope.createEvent = function(event) {
        event.date = new Date();
        $rootScope.showLoading();
        VictimService.createEvent($scope.victim._id, event).success(function() {
            /**
            * affiche l'icone de chargement
            */
            $rootScope.hideLoading();
            $scope.event={};
            $scope.loadVictim();
        }).error(function() {
            $rootScope.hideLoading();
        });
    };
    
    $scope.createService = function(service) {
        service.date = new Date();
        $rootScope.showLoading();
        VictimService.createService($scope.victim._id, service).success(function() {
            /**
            * affiche l'icone de chargement
            */
            $rootScope.hideLoading();
            $scope.service={};
            $scope.loadVictim();
        }).error(function() {
            $rootScope.hideLoading();
        });
    };
    
    $scope.updateVictim = function(victim) {
        $rootScope.showLoading();
        VictimService.update(victim).success(function() {
            /**
            * affiche l'icone de chargement
            */
            $rootScope.hideLoading();
            
            //$location.path("/victim");
            
        }).error(function() {
            $rootScope.hideLoading();
        });
    };
    
    $scope.initMap = function () {
        
        if(typeof $scope.victim.loc != 'undefined') {
            $scope.coordinates = $scope.victim.loc.coordinates;
        }
        else {
            $scope.coordinates = [0, 0];
        }
        
        $scope.majMarker = function(latlng) {
            if(typeof $scope.point === 'undefined') {
                $scope.point=L.marker(latlng, {
                    icon: $rootScope.icons['circle']
                });
                $scope.point.addTo($scope.mapLocalisation);
            } else {
                $scope.point.setLatLng(latlng);
            }
            $scope.victim.loc = { "type": "Point", "coordinates": [$scope.point.getLatLng().lat, $scope.point.getLatLng().lng] };
        };
        
        $scope.mapLocalisation = L.map('mapLocalisation', {
            center: $scope.coordinates,
            zoom: 5
        });

        L.tileLayer('http://{s}.tiles.mapbox.com/v3/examples.map-i875mjb7/{z}/{x}/{y}.png', {
            attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
            maxZoom: 18
        }).addTo($scope.mapLocalisation);

        $scope.mapLocalisation.on('click', function(e) {
            if($scope.canUpdateLoc){
                $scope.majMarker(e.latlng);
            }
        });
        
        if(typeof $scope.victim.loc != 'undefined') {
            $scope.majMarker(L.latLng($scope.victim.loc.coordinates[0], $scope.victim.loc.coordinates[1]));
        }
        
    };
    
    
    $scope.createVictim = function(victim) {
        console.log(victim);
        $rootScope.showLoading();
        VictimService.create(victim).success(function() {
            $rootScope.hideLoading();
            $location.path("/victim");
        }).error(function() {
            $rootScope.hideLoading();
        });
    };
});

