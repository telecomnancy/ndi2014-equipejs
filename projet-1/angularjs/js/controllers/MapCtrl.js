
appControllers.controller('MapCtrl', function($scope, $rootScope) {
    $scope.map = L.map('map', {
        center: [28.92163128242129, 66.181640625],
        zoom: 5
    });
    
    L.tileLayer('http://{s}.tiles.mapbox.com/v3/examples.map-i875mjb7/{z}/{x}/{y}.png', {
        attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
        maxZoom: 18
    }).addTo($scope.map);
    
    $scope.points = [];
    
    $scope.icons = {
        death: L.icon({
            iconUrl: 'img/map/icon/death.png',
            iconSize: [32, 32]
        }),
        coffin: L.icon({
            iconUrl: 'img/map/icon/coffin.png',
            iconSize: [32, 32]
        }),
        scull: L.icon({
            iconUrl: 'img/map/icon/scull.png',
            iconSize: [32, 32]
        }),
        types: ['death', 'coffin', 'scull']
    };
    $scope.listLatLng = [];
    $scope.map.on('click', function(e) {
        
        
        
        
        /*if(typeof $scope.polygon === 'undefined')
            $scope.polygon = L.polygon([e.latlng]);
        else
            $scope.polygon._latlngs.push(e.latlng);
        
        $scope.polygon.addTo($scope.map);*/
        
            /**
             * 
             */
        $scope.point=L.marker(e.latlng, {
            icon: $scope.icons[$scope.typeIcon]
        });
        //$scope.point.bindPopup('Oulalalala' + $scope.points.length);

        /**
         * 
         */
        $scope.points.push($scope.point);
        $scope.listLatLng.push($scope.point.getLatLng());
        
        if($scope.listLatLng.length == 5) {
            $scope.polygon = L.polygon($scope.listLatLng);
            $scope.polygon.bindPopup('ahahahahha');
            $scope.polygon.addTo($scope.map);
            $scope.listLatLng = [];
        }
        

        /**
         * 
         */
        $scope.point.addTo($scope.map);
    });
    
    
    
    
});
