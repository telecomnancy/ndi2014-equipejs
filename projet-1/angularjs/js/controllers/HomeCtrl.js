
appControllers.controller('HomeCtrl', function($scope, $rootScope, geolocation, ZoneService, VictimService) {
    
    $rootScope.showLoading();
    geolocation.getLocation().then(function(data){
        $rootScope.coords = {lat:data.coords.latitude, long:data.coords.longitude};
        $rootScope.$broadcast('map');
        $rootScope.hideLoading();
    });
    
    $scope.$on('map', function() {
        if($rootScope.coords === 'undefined') {
            $scope.coordinates = [28.92163128242129, 66.181640625]
        } else {
            $scope.coordinates = [$rootScope.coords.lat, $rootScope.coords.long];
        }
        
        $scope.map = L.map('map', {
            center: $scope.coordinates,
            zoom: 13
        });

        L.tileLayer('http://{s}.tiles.mapbox.com/v3/examples.map-i875mjb7/{z}/{x}/{y}.png', {
            attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
            maxZoom: 18
        }).addTo($scope.map);

        ZoneService.getAll().success(function(zones) {
            angular.forEach(zones, function(zone,key) {
                var lesCoordonnees = [];
                angular.forEach(zone.loc.coordinates, function(coordinate,key) {
                    if(coordinate == null) return
                    lesCoordonnees.push(L.latLng(coordinate[0],coordinate[1]));
                });
                var polygon = L.polygon(lesCoordonnees, {color: types_color[zone.type], fillColor: types_color[zone.type]}).addTo($scope.map);
                polygon.on('click', function(e) {
                    $scope.map.fitBounds(polygon.getBounds());
                });
                polygon.bindPopup(zone.name);
                polygon.addTo($scope.map);
            });
            
            VictimService.getAll().success(function(victims) {
                angular.forEach(victims, function(victim,key) {
                    latlng = L.latLng(victim.loc.coordinates[0], victim.loc.coordinates[1]);
                    $scope.point=L.marker(latlng, {
                        icon: $rootScope.icons['person']
                    });
                    content = victim.identity;
                    if(victim.services.length>0) content +="<br><br><b>Service</b>:";
                    angular.forEach(victim.services, function(service,key) {
                        content+="<br>- ";
                        if(service.provider != null) content+=service.provider.name;
                        content+=service.name;
                    });
                    
                    if(victim.events.length>0) content +="<br><br><b>Events</b>:";
                    angular.forEach(victim.events, function(event,key) {
                        content+="<br>- "+event.name;
                    });
                    $scope.point.bindPopup(content);
                    $scope.point.addTo($scope.map);
                });
                $rootScope.hideLoading();
            });
        }).error(function(){
            $rootScope.hideLoading();
        });
        
        
        ZoneService.getByCoordinates($rootScope.coords).success(function(data) {
            console.log(data);
        });
        
        var legend = L.control({position: 'bottomright'});

        legend.onAdd = function (map) {

            var div = L.DomUtil.create('div', 'info legend')

            // loop through our density intervals and generate a label with a colored square for each interval
            div.innerHTML += '<img src="img/map/icon/person.png"/> Victimes<br><br>';
            for (var i = 0; i < types_zone_index.length; i++) {
                div.innerHTML += '<i style="background: ' + types_color[types_zone_index[i]] + '"></i> ' +
                    types_zone[types_zone_index[i]] + '<br>';
            }

            return div;
        };

        legend.addTo($scope.map);
    });
    
});
