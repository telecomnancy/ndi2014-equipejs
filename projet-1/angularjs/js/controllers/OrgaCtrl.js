/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

appControllers.controller('OrgaCtrl', function ($scope, $rootScope, $location, $routeParams, OrganizationService) {

    $scope.orga = {};

    $scope.loadOrga = function () {
        OrganizationService.get($routeParams.id).success(function (orga) {
            console.log(orga);
            $scope.orga = orga;
        }).error(function () {
        });
    };

    $scope.updateOrga = function (orga) {
        OrganizationService.update(orga).success(function () {
            $location.path("/organizations/");

        }).error(function () {
        });
    };

    $scope.createOrga = function (orga) {
        OrganizationService.create(orga).success(function () {
            $location.path("/organizations/");
        }).error(function () {
        });
    };
});