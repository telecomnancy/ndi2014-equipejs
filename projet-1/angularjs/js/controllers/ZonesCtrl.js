/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

appControllers.controller('ZonesCtrl', function ($scope, $rootScope, ZoneService) {
    /**
     * initialisation zones
     */
    $scope.zones = [];
    
    $scope.types = types_zone_index;
    $scope.types_zone = types_zone;
     
    $scope.loadZone = function () {
        /**
         * Icone de chargement
         */
        $rootScope.showLoading();

        ZoneService.getAll().success(function (zones) {
            $scope.zones = zones;
            $rootScope.hideLoading();
        }).error(function () {
            $rootScope.hideLoading();
        });
    };
    
    $scope.deleteZone = function (index) {
        $rootScope.showLoading();
        ZoneService.delete($scope.zones[index]._id).success(function () {
            $rootScope.hideLoading();
            $scope.loadZone();
        }).error(function () {
            $rootScope.hideLoading();
        });
    };
    
    $scope.loadZone();
});
