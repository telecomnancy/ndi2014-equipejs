appServices.factory('AuthenticationService', function() {
    var auth = {
        isAuthenticated: false,
        isAdmin: false
    }

    return auth;
});

appServices.factory('TokenInterceptor', function ($q, $window, $location, AuthenticationService) {
    return {
        request: function (config) {
            config.headers = config.headers || {};
            if ($window.sessionStorage.token) {
                config.headers.Authorization = 'Bearer ' + $window.sessionStorage.token;
            }
            return config;
        },

        requestError: function(rejection) {
            return $q.reject(rejection);
        },

        /* Set Authentication.isAuthenticated to true if 200 received */
        response: function (response) {
            if (response != null && response.status == 200 && $window.sessionStorage.token && !AuthenticationService.isAuthenticated) {
                AuthenticationService.isAuthenticated = true;
            }
            return response || $q.when(response);
        },

        /* Revoke client authentication if 401 is received */
        responseError: function(rejection) {
            if (rejection != null && rejection.status === 401 && ($window.sessionStorage.token || AuthenticationService.isAuthenticated)) {
                delete $window.sessionStorage.token;
                AuthenticationService.isAuthenticated = false;
                $location.path("/admin/login");
            }

            return $q.reject(rejection);
        }
    };
});


//Service for the user functionnalities
appServices.factory('UserService', function ($http) {
    return {
        //Log a user
        signIn: function(mail, password) {
            return $http.post(options.api.base_url + '/signin/', $.param({identity: mail, password: password}), { headers: { 'Content-Type': 'application/x-www-form-urlencoded'}});
        },

        //Log out a user
        logOut: function() {
            return $http.get(options.api.base_url + '/logout/');
        },
        
        //Get the profile of an user
        getMe : function(){
            return $http.get(options.api.base_url + '/user/me/');
        },

        //Get all the users
        getUsers: function(){
            return $http.get(options.api.base_url + '/users/');
        },
        getUser: function(user_id){
            return $http.get(options.api.base_url + '/user/' + user_id + '/');
        },
        putUser: function(user){
            return $http.put(options.api.base_url + '/user/', user);
        },
        postUser: function(user){
            return $http.post(options.api.base_url + '/user/', user);
        },
        deleteUser: function(user_id){
            return $http.delete(options.api.base_url + '/user/', {params: { id: user_id}});
        }
    }
});

appServices.factory('NewsService', function($http, $q){
    return {
        getNewss: function(){
            return $http.get(options.api.base_url + '/news');
        },
        getNews: function(news_id){
            return $http.get(options.api.base_url + '/news/' + news_id);
        },
        putNews: function(news){
            return $http.put(options.api.base_url + '/news', news);
        },
        deleteNews: function(news_id){
            return $http.delete(options.api.base_url + '/news', {params: { id: news_id}});
        },
        postNews: function(news){
            return $http.post(options.api.base_url + '/news', news);
        }
    };
});

appServices.factory('SearchService', function($http){
    return{
        searchVictims : function(infos){
            return $http.get(options.api.base_url + '/victims/', {params : infos});
        }
    }
});

appServices.factory('VictimService', function($http){
    return{

        /********** CRUD *********/

        get : function(id){
            return $http.get(options.api.base_url + '/victims/' + id + '/');
        },

        delete : function(id){
            return $http.delete(options.api.base_url + '/victims/' + id + '/');
        },

        update : function(infos){
            return $http.put(options.api.base_url + '/victims/' + infos._id + '/', infos);
        },

        /********** LIST *********/

        getAll : function(){
            return $http.get(options.api.base_url + '/victims/');
        },

        create : function(infos){
            return $http.post(options.api.base_url + '/victims/', infos);
        },

        /********** EVENTS *********/

        createEvent : function(id, event){
            return $http.post(options.api.base_url + '/victims/' + id + '/events/', event);
        },

        /********** SERVICE *********/

        createService : function(id, serv){
            return $http.post(options.api.base_url + '/victims/' + id + '/services/', serv);
        },

        /********** ZONE *********/

        updateZone : function(id, zone){
            return $http.put(options.api.base_url + '/victims/' + id + '/zone/', zone);
        },

        /********** MESSAGES ***********/

        sendMess : function(id, data){
            return $http.post(options.api.base_url + '/victims/' + id + '/messages/', data);
        }
    }
});

appServices.factory('ZoneService', function($http){
    return{

        /********** CRUD *********/

        get : function(id){
            return $http.get(options.api.base_url + '/zones/' + id + '/');
        },

        //Pour chercher la zone de la personne. coordinate -> lat, lon
        getByCoordinates : function(coordinates){
            return $http.get(options.api.base_url + '/zones/', {params: coordinates});
        },

        delete : function(id){
            return $http.delete(options.api.base_url + '/zones/' + id + '/');
        },

        update : function(infos){
            return $http.put(options.api.base_url + '/zones/' + infos._id + '/', infos);
        },        

        /********** LIST *********/

        getAll : function(){
            return $http.get(options.api.base_url + '/zones/');
        },

        create : function(infos){
            return $http.post(options.api.base_url + '/zones/', infos);
        },

        getByType : function(type){
            return $http.get(options.api.base_url + '/zones/', {params : type});
        },

        getByName : function(name){
            return $http.get(options.api.base_url + '/zones/', {params : name});
        },

        /********** RULE *********/

        createRule : function(id, rule){
            return $http.post(options.api.base_url + '/zones/' + id + '/rules/', rule);
        },

        delRule : function(id, idrule){
            return $http.delete(options.api.base_url + '/zones/' + id + '/rules/' + idrule + '/');
        },

        /********** STOCK *********/
        //stock : name, type, quantity
        createStock : function(id, stock){
            return $http.post(options.api.base_url + '/zones/' + id +'/stocks/', stock);
        },

        updateStock : function(id, nameStock, data){
            return $http.put(options.api.base_url + '/zones/' + id + '/stocks/' +nameStock, data);
        }
    }
});

appServices.factory('RuleService', function(){
    return{
        //point -> lat, lon
        get : function(point){
            return $http.get(options.api.base_url + '/rules/', {params: point});
        }
    }
});

appServices.factory('OrganizationService', function($http){
    return{
        /********** CRUD *********/
        get : function(id){
            return $http.get(options.api.base_url + '/organizations/' + id + '/');
        },

        update : function(id, data){
            return $http.put(options.api.base_url + '/organizations/' + id + '/', data);
        },

        delete : function(id){
            return $http.delete(options.api.base_url + '/organizations/' + id + '/');
        },

        /********** LIST *********/
        getAll : function(){
            return $http.get(options.api.base_url + '/organizations/');
        },

        create : function(org){
            return $http.post(options.api.base_url + '/organizations/', org);
        }
    }
});