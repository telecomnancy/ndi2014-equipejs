
appControllers.controller('AppCtrl', function($scope, $rootScope, $http, $timeout, UserService, AuthenticationService, $window, $location) {
    
    
    /**
     * Afficher le loading
     */
    $rootScope.showLoading = function () {
        $rootScope.loading = true;
    };
    
    /**
     * Cacher le loading
     */
    $rootScope.hideLoading = function () {
        $rootScope.loading = false;
    };
    
    /**
     * Check "est connecté"
     */
    $rootScope.isLogin = function(){
        return AuthenticationService.isAuthenticated;
    };
   
    $rootScope.icons = {
        circle: L.icon({
            iconUrl: 'img/map/icon/circle.png',
            iconSize: [32, 32]
        }),
        death: L.icon({
            iconUrl: 'img/map/icon/death.png',
            iconSize: [32, 32]
        }),
        coffin: L.icon({
            iconUrl: 'img/map/icon/coffin.png',
            iconSize: [32, 32]
        }),
        scull: L.icon({
            iconUrl: 'img/map/icon/scull.png',
            iconSize: [32, 32]
        }),
        person: L.icon({
            iconUrl: 'img/map/icon/person.png',
            iconSize: [16, 32]
        }),
        types: ['death', 'coffin', 'scull']
    };
})

.controller('LogOutCtrl', function($scope, $rootScope, $location, $timeout, $window, AuthenticationService, UserService){
    
    if (AuthenticationService.isAuthenticated) {

        $rootScope.loading = true;

        AuthenticationService.isAuthenticated = false;
        delete $window.sessionStorage.token;
        $timeout(function() {
            $rootScope.loading = false;
            $location.path("/");
        }, 1000);

    }
    else {
        $location.path("/");
    }   
    
})

.controller('AuthCtrl', function($scope, $rootScope, $timeout, $window, $location, UserService, AuthenticationService) {
    
    console.log("AuthCtrl");
    
    if($rootScope.isLogin()){
        $location.path('/');
    }
    
    /**
     * Méthode de connexion appeler le serviceUser
     */
    $scope.doLogin = function(mail, password) {
          
        if (mail != null && password != null) {
            
            $rootScope.loading = true;
            
            UserService.signIn(mail, password).success(function(data) {
                $scope.message={status:true, content:'Connexion effectuée'};
                AuthenticationService.isAuthenticated = true;
                $window.sessionStorage.token = data.token;
                $rootScope.myUser = data.user;
                $timeout(function() {
                    delete $scope.message;
                    $rootScope.loading = false;
                    $location.path('/');
                }, 1000);
            }).error(function(status, data) {
                console.log(status);
                console.log(data);
                $rootScope.loading = false;
                $scope.message={status:false, content:'Mail ou mot de passe invalide'};
                $timeout(function() {
                    delete $scope.message;
                }, 1000);
            });
        }
    };
})

.controller('UserCtrl', function($scope, $rootScope, $location, $routeParams, UserService){
    
    
    $scope.loadUser = function(){
        /**
         * On affiche le loading
         */
        $rootScope.loading = true;
        
        /**
         * Récupération de le user choisi
         */
        UserService.getUser($routeParams.id).success(function(user){
            
            /**
             * Enregistrement de le user
             */
            $scope.user = user;

            /**
             * On cache le loading
             */
            $rootScope.loading = false;

        }).error(function(){

           /**
            * On cache le loading
            */
           $rootScope.loading = false;

           /**
            * Si une erreur survient on le redirige !
            */
           $location.path('/user');
        });
    };
    
    $scope.updateUser = function(user) {
        
        /**
         * On affiche le loading
         */
        $rootScope.loading = true;
        
        
        UserService.putUser(user).success(function(){
            
            /**
             * On cache le loading
             */
            $rootScope.loading = false;
            
            /**
             * Redirection
             */
            $location.path('/user');
            
        }).error(function(){
        
            /**
             * On cache le loading
             */
            $rootScope.loading = false;
        });
    };
    
    $scope.createUser = function(user) {
        
        /**
         * On affiche le loading
         */
        $rootScope.loading = true;
        
        UserService.postUser(user).success(function(){
        
            /**
             * On cache le loading
             */
            $rootScope.loading = false;
            
            /**
             * Redirection
             */
            $location.path('/user');
            
        }).error(function(){
        
            /**
             * On cache le loading
             */
            $rootScope.loading = false;
        });
    };
    
})

.controller('UsersCtrl', function($scope, $rootScope, $location, UserService){
    $rootScope.sidebar = 'user';
    
    $scope.loadUsers = function(){
        /**
         * On affiche le loading
         */
        $rootScope.loading = true;

        /**
         * On charge les users !
         */
        UserService.getUsers().success(function(users){

            /**
             * On enregistre tous les users
             */
            $scope.users = users;

            /**
             * On cache le loading
             */
            $rootScope.loading = false;
        }).error(function(){

            /**
             * Si une erreur survient on cache le loading
             */
            $rootScope.loading = false;
        });
    };
    
    $scope.deleteUser = function(user_id) {
        
        /**
         * On affiche le loading
         */
        $rootScope.loading = true;
        
        
        UserService.deleteUser(user_id).success(function(){
            
            /**
             * On cache le loading
             */
            $rootScope.loading = false;
            
            /**
             * Rechargement des users
             */
            $scope.loadUsers();
            
        }).error(function(){
        
            /**
             * On cache le loading
             */
            $rootScope.loading = false;
        });
    };
    
    $scope.loadUsers();
})

.controller('NewssCtrl', function($scope, $rootScope, NewsService){
    $rootScope.sidebar = 'news';
    
    $scope.loadNewss = function(){
        /**
         * On affiche le loading
         */
        $rootScope.loading = true;

        /**
         * On charge les thèmes !
         */
        NewsService.getNewss().success(function(newss){

            /**
             * On enregistre tous les thèmes
             */
            $scope.newss = newss;

            /**
             * On cache le loading
             */
            $rootScope.loading = false;
        }).error(function(){

            /**
             * Si une erreur survient on cache le loading
             */
            $rootScope.loading = false;
        });
    };
    
    $scope.deleteNews = function(news_id) {
        
        /**
         * On affiche le loading
         */
        $rootScope.loading = true;
        
        
        NewsService.deleteNews(news_id).success(function(){
            
            /**
             * On cache le loading
             */
            $rootScope.loading = false;
            
            /**
             * Rechargement des newss
             */
            $scope.loadNewss();
            
        }).error(function(){
        
            /**
             * On cache le loading
             */
            $rootScope.loading = false;
        });
    };
    
    $scope.loadNewss();
})

.controller('NewsCtrl', function($scope, $rootScope, $routeParams, $location, NewsService, SchoolService){
    $rootScope.sidebar = 'news';
    
    /**
     * Chargement schools
     */
    SchoolService.getSchools().success(function(schools){
        $scope.schools = schools;
    });
    
    $scope.loadNews = function(){
        /**
         * On affiche le loading
         */
        $rootScope.loading = true;
        
        /**
         * Récupération du thème choisi
         */
        NewsService.getNews($routeParams.id).success(function(data){

           /**
            * Enregistrement du news
            */
           $scope.news = data;

           /**
            * On cache le loading
            */
           $rootScope.loading = false;

        }).error(function(){

           /**
            * On cache le loading
            */
           $rootScope.loading = false;

           /**
            * Si une erreur survient on le redirige !
            */
           $location.path('/news');
        });
    };
    
    $scope.updateNews = function(news) {
        
        /**
         * On affiche le loading
         */
        $rootScope.loading = true;
        
        NewsService.putNews(news).success(function(){
            
            /**
             * On cache le loading
             */
            $rootScope.loading = false;
            
            /**
             * Redirection
             */
            $location.path('/news');
            
        }).error(function(){
        
            /**
             * On cache le loading
             */
            $rootScope.loading = false;
        });
    };
    
    $scope.createNews = function(news) {
        
        /**
         * On affiche le loading
         */
        $rootScope.loading = true;
        
        NewsService.postNews(news).success(function(){
            
            /**
             * On cache le loading
             */
            $rootScope.loading = false;
            
            /**
             * Redirection
             */
            $location.path('/news');
            
        }).error(function(){
        
            /**
             * On cache le loading
             */
            $rootScope.loading = false;
        });
    };
});