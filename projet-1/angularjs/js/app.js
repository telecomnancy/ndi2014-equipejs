// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
'use strict';

var app = angular.module('app', ['ngRoute', 'angular-datepicker', 'geolocation', 'appControllers', 'appServices', 'appDirectives']);
var types_zone = {"CAMPS": "Camps de réfugiés", "GUERRE": "Zone de conflit", "EPIDEMIE" : "Zone d'épidémie", "AUTRES" : "Autre"};
var types_color = {"CAMPS": "#31E1C9", "GUERRE": "#E80000", "EPIDEMIE": "#00D423", "AUTRES": "#7A827C"};
var types_zone_index = ["CAMPS", "GUERRE", "EPIDEMIE", "AUTRES"];

var appServices = angular.module('appServices', []);
var appControllers = angular.module('appControllers', []);
var appDirectives = angular.module('appDirectives', []);

var options = {};
options.api = {};
options.api.base_url = "http://193.54.15.144:80";

options.api_google = {};
options.api_google.base_url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?sensor=true&radius=100&key=AIzaSyBO48_EO6t53tWqx9lyx-TV10pfaNpbjGk";

app.run(function($rootScope, $window, $location, $route, UserService, AuthenticationService, geolocation) {
    
    
    
    $rootScope.$on("$routeChangeStart", function(event, nextRoute, currentRoute) {
        
        //redirect only if both isAuthenticated is false and no token is set
        if (nextRoute != null && nextRoute.access != null && nextRoute.access.requiredAuthentication 
            && !AuthenticationService.isAuthenticated && !$window.sessionStorage.token) {
            console.log($location.path)
            $location.path("/login");
            $route.reload();
        }
        
        if($window.sessionStorage.token){
            UserService.getMe().success(function(data){
                $rootScope.myUser = data;
                AuthenticationService.isAuthenticated=true;
            }).error(function(){
                //delete $window.sessionStorage.token;
                console.log('error login');
            });
        }
    });
})

.config(function($routeProvider, $httpProvider) {
    $httpProvider.interceptors.push('TokenInterceptor');
    $routeProvider.
            
        /**
         * Accueil
         */
        when('/', {
            templateUrl: 'templates/home.html',
            controller: 'HomeCtrl'
        }).
                
        /**
         * Login & Logout
         */
        when('/login', {
            templateUrl: 'templates/login.html',
            controller: 'AuthCtrl'
        }).
        when('/logout', {
            templateUrl: 'templates/logout.html',
            controller: 'LogOutCtrl'
        }).
                
        /**
         * News
         */
        when('/news', {
            templateUrl: 'templates/newss.html',
            controller: 'NewssCtrl',
            access: { requiredAuthentication: true }
        }).
                
        /**
         * nav
         */
        when('/nav', {
            templateUrl: 'templates/nav.html',
            access: { requiredAuthentication: true }
        }).
                
        /**
         * Map
         */
        when('/map', {
            templateUrl: 'templates/map.html',
            controller: 'MapCtrl'
        }).
        
        /**
         * Search
         */
            
        when('/search', {
            templateUrl: 'templates/search.html',
            controller: 'SearchCtrl'
        }).
        
        /**
         * Victim(s)
         */    
            
        when('/victim', {
            templateUrl: 'templates/victims.html',
            controller: 'VictimsCtrl'
        }).
        
        when('/victim/new', {
            templateUrl: 'templates/victim-new.html',
            controller: 'VictimCtrl',
            access: { requiredAuthentication: true }
        }).
                
        when('/victim/:id', {
            templateUrl: 'templates/victim.html',
            controller: 'VictimCtrl'
        }).
        
        when('/victim/:id/update', {
            templateUrl: 'templates/victim-update.html',
            controller: 'VictimCtrl',
            access: { requiredAuthentication: true }
        }).
        
        /**
         * Zone(s)
         */
        
        when('/zone', {
            templateUrl: 'templates/zones.html',
            controller: 'ZonesCtrl',
            access: { requiredAuthentication: true }
        }).
        
        when('/zone/new', {
            templateUrl: 'templates/zone-new.html',
            controller: 'ZoneCtrl',
            access: { requiredAuthentication: true }
        }).
                
        when('/zone/:id', {
            templateUrl: 'templates/zone.html',
            controller: 'ZoneCtrl',
            access: { requiredAuthentication: true }
        }).
        
        when('/zone/:id/update', {
            templateUrl: 'templates/zone-update.html',
            controller: 'ZoneCtrl',
            access: { requiredAuthentication: true }
        }).
        
        /**
         * Orgas
         */
        
        when('/organizations/', {
            templateUrl: 'templates/orgas.html',
            controller: 'OrgasCtrl',
            access: { requiredAuthentication: true }
        }).
        
        when('/organizations/new', {
            templateUrl: 'templates/orga-new.html',
            controller: 'OrgaCtrl',
            access: { requiredAuthentication: true }
        }).
        
        when('/organizations/:id', {
            templateUrl: 'templates/orga.html',
            controller: 'OrgaCtrl',
            access: { requiredAuthentication: true }
        }).
        
        when('/organizations/:id/update', {
            templateUrl: 'templates/orga-update.html',
            controller: 'OrgaCtrl',
            access: { requiredAuthentication: true }
        });
        
        
        
        
        
        
       $routeProvider.otherwise({ redirectTo: '/' });
});

